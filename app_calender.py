from tkinter import *
import datetime
from datetime import datetime as dt
from persiantools.jdatetime import JalaliDate as jd
from tkinter import messagebox
import calendar as cd

root = Tk()
root.geometry('600x500')
root.config(bg='#A9FFF5')
root.title('Calender')

class App_calender():    
    def __init__(self,Month,Days,title,text):
        culcuting.destroy()
        converting.destroy()
        win.destroy()
        showing.destroy()
        distancing.destroy()
        
        self.Month = Month
        self.Days = Days
        self.Year_s = StringVar()
        self.Month_s = StringVar()
        self.Days_s = StringVar()
        self.title = title
        self.text = text
        self.user_year = Entry(root,font=('Times New Romer',20),width=7,bg='#00FFB2')
        self.user_year.place(x=160,y=175)
        self.user_month = Spinbox(root,value=self.Month,font=('Times New Romer',20),width=6,bg='#00FFB2')
        self.user_month.place(x=160,y=245)
        self.user_day = Spinbox(root,value=Days,font=('Times New Romer',20),width=6,bg='#00FFB2')
        self.user_day.place(x=160,y=315)
        
        Frame(root,bg='#00FFB2',width=260,height=220).place(x=300,y=170)
        Frame(root,bg='#A9FFF5',width=254,height=214).place(x=303,y=173)   
        
        Label(root,text=self.title,font=('Times New Romer',40),bg='#00FFB2').place(x=130,y=25)
        Label(root,text='Year',font=('Times New Romer',28),bg='#A9FFF5').place(x=30,y=170)
        Label(root,text='Month',font=('Times New Romer',28),bg='#A9FFF5').place(x=30,y=240)
        Label(root,text='Day',font=('Times New Romer',28),bg='#A9FFF5').place(x=30,y=310)
        Label(root,text=self.text,font=('Times New Romer',35),fg='#00ffb2',bg='#a9fff5').place(x=290,y=140)
        Label(root,textvariable=self.Year_s,font=('Times New Romer',30),bg='#A9FFF5').place(x=340,y=200)
        Label(root,textvariable=self.Month_s,font=('Times New Romer',30),bg='#A9FFF5').place(x=340,y=260)
        Label(root,textvariable=self.Days_s,font=('Times New Romer',30),bg='#A9FFF5').place(x=340,y=320)
        
        if self.title =='Calculate Age':
            if Month==persian_month :
                Button(root,text=self.title,font=('Times New Romer',28),width=13,height=1,bg='#00FFB2',fg='black',borderwidth=0,command= lambda : self.Calculate_Age(jd.today())).place(x=145,y=400)
            else :
                Button(root,text=self.title,font=('Times New Romer',28),width=13,height=1,bg='#00FFB2',fg='black',borderwidth=0,command= lambda : self.Calculate_Age(datetime.date.today())).place(x=145,y=400)
            Label(root,text='please enter your date of birth ',font=('Times New Romer',20),bg='#A9FFF5').place(x=100,y=90)
        else :
            if Month==persian_month :
                Button(root,text=self.title,font=('Times New Romer',28),width=13,height=1,bg='#00FFB2',fg='black',borderwidth=0,command=self.converting_Date_P).place(x=145,y=400)
            else :
                Button(root,text=self.title,font=('Times New Romer',28),width=13,height=1,bg='#00FFB2',fg='black',borderwidth=0,command=self.converting_Date_E).place(x=145,y=400)
            Label(root,text='please enter a date',font=('Times New Romer',20),bg='#A9FFF5').place(x=150,y=90)

        Button(root,text='back',font=('Times New Romer',20),width=5,height=1,bg='#A9FFF5',fg='#00FFB2',borderwidth=0,command=screen_1).place(x=15,y=430)
    

class Screen_2(App_calender):
    def converting_Date_E(self):
        try :
            date = jd.to_jalali(int(self.user_year.get()),(self.Month.index(self.user_month.get())+1),int(self.user_day.get()))
        except:
            return messagebox.showinfo(title="Error", message="Please enter the year.")
        self.Year_s.set(f'{date.year} year')
        self.Month_s.set(f'{date.month} month')
        self.Days_s.set(f'{date.day} day')   
    
    def converting_Date_P(self):
        try :
            date = jd(int(self.user_year.get()),(self.Month.index(self.user_month.get())+1),int(self.user_day.get())).to_gregorian()   
        except ValueError:
            return messagebox.showinfo(title="Error", message="Please enter the year.")
        self.Year_s.set(f'{date.year} year')
        self.Month_s.set(f'{date.month} month')
        self.Days_s.set(f'{date.day} day')   

class Screen_3(App_calender):
    def Calculate_Age(self,today):
        if self.user_year.get() == "":
            messagebox.showinfo(title="Error", message="Please enter the year.")
        try :
            if int(self.user_year.get()) > today.year:
               return messagebox.showinfo(title="Error", message=f"The year should not be older that {today.year}")
        except:
                return messagebox.showinfo(title="Error", message="Please enter the year.")

        if (self.Month.index(self.user_month.get())+1) == 12:
            self.user_year = int(self.user_year.get())+1
            self.user_month = 1
            self.user_day = int(self.user_day.get()) - 31
            self.year = today.year-self.user_year
            self.month = today.month-self.user_month
            self.day = today.day-self.user_day
            if self.day > 31 : 
                self.day = self.day-31
                self.month +=1
        else:
            self.year = today.year - int(self.user_year.get())
            self.month = today.month - (self.Month.index(self.user_month.get())+1)
            self.day = today.day - int(self.user_day.get())
            if self.month < 0 :
                self.month= 12 + self.month
                self.year -= 1
            if self.day < 0 :
                self.day= 31 + self.day
                self.month -=1
        self.Year_s.set(f'{self.year} years')
        self.Month_s.set(f'{self.month} months')
        self.Days_s.set(f'{self.day} days')

class App_Calender2():
    def __init__(self,Month,Days,title,text):
        culcuting.destroy()
        converting.destroy()
        win.destroy()
        showing.destroy()
        distancing.destroy()
        
        self.Month = Month
        self.Days = Days
        self.title = title
        self.text = text
        self.Year_s = StringVar()
        self.Month_s = StringVar()
        self.Days_s = StringVar()
        
        Frame(root,bg='#00FFB2',width=556,height=110).place(x=20,y=280)
        Frame(root,bg='#A9FFF5',width=550,height=104).place(x=23,y=283)
        #Date 1
        self.user_year1 = Entry(root,font=('Times New Romer',20),width=7,bg='#00FFB2')
        self.user_year1.place(x=60,y=190)
        self.user_month1 = Spinbox(root,value=self.Month,font=('Times New Romer',20),width=6,bg='#00FFB2')
        self.user_month1.place(x=240,y=190)
        self.user_day1 = Spinbox(root,value=Days,font=('Times New Romer',20),width=6,bg='#00FFB2')
        self.user_day1.place(x=410,y=190)
        Label(root,text=self.title,font=('Times New Romer',40),bg='#00FFB2').place(x=90,y=25)
        Label(root,text='Year',font=('Times New Romer',28),bg='#A9FFF5').place(x=70,y=135)
        Label(root,text='Month',font=('Times New Romer',28),bg='#A9FFF5').place(x=240,y=135)
        Label(root,text='Day',font=('Times New Romer',28),bg='#A9FFF5').place(x=430,y=135)
        #Date 2
        self.user_year = Entry(root,font=('Times New Romer',20),width=7,bg='#00FFB2')
        self.user_year.place(x=60,y=230)
        self.user_month = Spinbox(root,value=self.Month,font=('Times New Romer',20),width=6,bg='#00FFB2')
        self.user_month.place(x=240,y=230)
        self.user_day = Spinbox(root,value=Days,font=('Times New Romer',20),width=6,bg='#00FFB2')
        self.user_day.place(x=410,y=230)
       
        Label(root,textvariable=self.Year_s,font=('Times New Romer',30),bg='#A9FFF5').place(x=30,y=300)
        Label(root,textvariable=self.Month_s,font=('Times New Romer',30),bg='#A9FFF5').place(x=190,y=300)
        Label(root,textvariable=self.Days_s,font=('Times New Romer',30),bg='#A9FFF5').place(x=390,y=300)
        
        Button(root,text=self.title,font=('Times New Romer',28),width=13,height=1,bg='#00FFB2',fg='black',borderwidth=0,command= self.distance_Date).place(x=145,y=400)
        Label(root,text='please enter two date',font=('Times New Romer',20),bg='#A9FFF5').place(x=160,y=90)
        Button(root,text='back',font=('Times New Romer',20),width=5,height=1,bg='#A9FFF5',fg='#00FFB2',borderwidth=0,command=screen_1).place(x=15,y=430)

class Screen_4(App_Calender2):
    def distance_Date(self):        
        if self.user_year.get() == "" or self.user_year1.get()=="":
            messagebox.showinfo(title="Error", message="Please enter the year.")
      
        if (self.Month.index(self.user_month1.get())+1) == 12:
            self.user_year1 = int(self.user_year1.get())+1
            self.user_month1 = 1
            self.user_day1 = int(self.user_day1.get()) - 31
            self.year = int(self.user_year.get()) - self.user_year1
            self.month = (self.Month.index(self.user_month.get())+1) - self.user_month1
            self.day = int(self.user_day.get()) - self.user_day1
            if self.day > 31 : 
                self.day = self.day-31
                self.month +=1
        else:
            self.year = int(self.user_year.get())- int(self.user_year1.get())
            self.month = (self.Month.index(self.user_month.get())+1) - (self.Month.index(self.user_month1.get())+1)
            self.day = int(self.user_day.get())- int(self.user_day1.get())
            if self.month < 0 :
                self.month= 12 + self.month
                self.year -= 1
            if self.day < 0 :
                self.day= 31 + self.day
                self.month -=1
        self.Year_s.set(f'{abs(self.year)} years')
        self.Month_s.set(f'{self.month} months')
        self.Days_s.set(f'{self.day} days')


def show_calender():
    converting.destroy()
    culcuting.destroy()
    showing.destroy()
    distancing.destroy()
    
    calender = StringVar()
    global month,year
    month = datetime.date.today().month
    year = datetime.date.today().year
    calender.set(cd.month(2023,month))
    
    def calender_f():
        global month,year
        if month==12:
            month = 1
            year+=1
        else:
            month+=1
        calender.set(cd.month(year,month))
        
    def calender_b():
        global month,year
        if month==1:
            month = 12
            year-=1
        else:
            month-=1
        calender.set(cd.month(year,month))

    Label(root,textvariable=calender,font=('Times New Romer',34),bg='#A9FFF5').place(x=50,y=60)
    Button(root,text='<-',font=('Times New Romer',35),width=4,borderwidth=0,bg='#A9FFF5',command=calender_b).place(x=10,y=20)
    Button(root,text='->',font=('Times New Romer',35),width=4,borderwidth=0,bg='#A9FFF5',command=calender_f).place(x=470,y=20)
    Button(root,text='back',font=('Times New Romer',20),width=5,height=1,bg='#A9FFF5',fg='#00FFB2',borderwidth=0,command=screen_1).place(x=15,y=430)
   

def new_screen(screen,title,text):
    global win
    win = Toplevel(root)
    win.geometry('300x200')
    win.config(bg='#00FFB2')
    Button(win,text='Persian Date',font=('Times New Romer',24),width=13,height=1,bg='#A9FFF5',fg='black',borderwidth=0,command= lambda :  screen(persian_month,day,title,text)).place(x=25,y=30)
    Button(win,text='English Date',font=('Times New Romer',24),width=13,height=1,bg='#A9FFF5',fg='black',borderwidth=0,command= lambda :  screen(english_month,day,title,text)).place(x=25,y=110)
   

english_month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
persian_month = ['Far','Ord','Khor','Tir','Mord','Shahr','Mehr','Aban','Azar','Dey','Bah','Esf']
day = [i for i in range(1,32)]

def screen_1():
    global converting,culcuting,showing,distancing
    Frame(root,width=600,height=500,bg ='#A9FFF5').place(x=0,y=0)
    culcuting = Button(root,text='Conver Date',font=('Times New Romer',28),width=13,height=1,bg='#00FFB2',fg='black',borderwidth=0,command= lambda : new_screen(Screen_2,'Conver Date','Date'))
    culcuting.place(x=145,y=50)
    converting = Button(root,text='Calculate Age',font=('Times New Romer',28),width=13,height=1,bg='#00FFB2',fg='black',borderwidth=0,command= lambda : new_screen(Screen_3,'Calculate Age','Age'))
    converting.place(x=145,y=150)
    showing= Button(root,text='Show Calender',font=('Times New Romer',28),width=13,height=1,bg='#00ffb2',fg='black',borderwidth=0,command= show_calender)
    showing.place(x=145,y=250)
    distancing= Button(root,text='Distance two date',font=('Times New Romer',28),width=13,height=1,bg='#00ffb2',fg='black',borderwidth=0,command=lambda: new_screen(Screen_4,'Distance two date','Date'))
    distancing.place(x=145,y=350)
screen_1()



mainloop()